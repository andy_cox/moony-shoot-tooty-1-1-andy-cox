﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// EnemyMovement handles all of the movement specifc state and behaviour for the enemy.
/// </summary>
public class EnemyMovement : EngineBase
{
   public void Accelerate(Vector2 direction)
    {
        base.Accelerate(direction);
    }
}
