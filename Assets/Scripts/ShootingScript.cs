﻿using UnityEngine;
using System.Collections;

public class ShootingScript : MonoBehaviour
{

    [SerializeField]
    private GameObject bullet;

    private float lastFiredTime = 0f;

    private float fireDelay = 1f;

    private float bulletOffset = 2f;

    public void ShootBullet()
    {

        bulletOffset = GetComponent<Renderer>().bounds.size.y / 2
            + bullet.GetComponent<Renderer>().bounds.size.y / 2;

        float currentTime = Time.time;

       // Debug.Log($"currentTime: {currentTime}, lastFiredTime: {lastFiredTime}, fireDelay: {fireDelay}");

        // Have a delay so we don't shoot too many bullets
        if (currentTime - lastFiredTime > fireDelay)
        {
            Vector2 spawnPosition = new Vector2(transform.position.x, transform.position.y + bulletOffset);

            Instantiate(bullet, spawnPosition, transform.rotation);

            lastFiredTime = currentTime;  
        }
    }

    /*
    public float SampleMethod(int number) {
        return number;

        float CurrentTime = Time.time;

        // Have a delay so we don't shoot too many bullets
        if (CurrentTime - lastFiredTime > fireDelay)
        {
            Vector2 spawnPosition = new Vector2(transform.position.x, transform.position.y + bulletOffset);

            Instantiate(bullet, spawnPosition, transform.rotation);

            lastFiredTime = CurrentTime;
        }

    }
    */
}
